# tags-base

## Summary

Provides the ability for consumers to consistently tag all resources in AWS.

## Resources

The following tags are created by this module:
* ManagedByTerraform
* Application
* Environment
* Lifespan

## Cost

Using this module on its own doesn't cost anything.

## Inputs

| Name        | Description                                                                                                                                         | Type          | Default | Required |
|:------------|:----------------------------------------------------------------------------------------------------------------------------------------------------|:--------------|:--------|:---------|
| application | Name of the application the resource(s) will be a part of.                                                                                          | `string`      | -       | yes      |
| environment | Name of the environment the resource(s) will be a part of. Used to lookup the VPC by name.                                                          | `string`      | -       | yes      |
| lifespan    | The intended lifespan of the resource(s). Acceptable values: `temporary` - Can be destroyed after use or testing; `permanent` - Cannot be destroyed | `string`      | -       | yes      |
| tags        | Additional tags to attach to all resources.                                                                                                         | `map(string)` | `{}`    | no       |

## Outputs

| Name | Description                                                                   |
|:-----|:------------------------------------------------------------------------------|
| tags | A combined list of tags including input tags and tags created by this module. |

## Examples

### Simple Usage

```terraform
module "tags_base" {
  source = "git::https://gitlab.com/bixbots/cloud/terraform-modules/tags-base.git?ref=v1"

  application = "example_app"
  environment = "example_env"
  lifespan    = "temporary"

  tags = {
    "OtherTag1Name" = "OtherTag1Value"
  }
}

resource "aws_instance" "example" {
  # details omitted for brevity
  # ...

  tags = merge(module.tags_base.tags, {
    "OtherTag2Name" = "OtherTag2Value"
  })
}
```

## Version History

* v1 - Initial Release
