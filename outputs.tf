output "tags" {
  description = "A combined list of tags including input tags and tags created by this module."
  value = merge(var.tags, {
    ManagedByTerraform = "true"
    Application        = var.application
    Environment        = var.environment
    Lifespan           = var.lifespan
  })
}
