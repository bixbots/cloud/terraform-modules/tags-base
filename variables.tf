variable "application" {
  type        = string
  description = "Name of the application the resource(s) will be a part of."
}

variable "environment" {
  type        = string
  description = "Name of the environment the resource(s) will be a part of."
}

variable "lifespan" {
  type        = string
  description = <<EOF
    The intended lifespan of the resource(s). Acceptable values:
    - `temporary` - Can be destroyed after use or testing
    - `permanent` - Cannot be destroyed
  EOF
}

variable "tags" {
  type        = map(string)
  default     = {}
  description = "(Optional) Additional tags to attach to all resources."
}
